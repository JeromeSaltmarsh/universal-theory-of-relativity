***I Hypothesise that there is no such thing as gravity and that matter not only curves the space around it but swallows it up to experience time***

by Jerome Saltmarsh



According to Newton's understanding of space, space is an infinite, fixed symetric three dimensional coordinate system. 

Einstein discovered two important qualities that did not coencide with Newton's model. 

The first was that space was not perfectly symetric but that it was influenced by matter which pulled and distorted it.

The second was that he realized that space and time were inextricably connected, so much so in fact that he combined the two into a single word and called it spacetime. 

He showed us that gravity was not a force rather a biproduct of the effect that massive objects had on their surrounding spacetime.

***Matter and spacetime***

I hypothesize that if time is flowing, because space is inextricably connected to it, space must be flowing as well.

I believe that this flow is created by matter which is constantly consuming the spactime around it like water getting sucked into a drain.

The more massive and dense the matter the more space is consumes. 

Matter must consume space in order to experience time.

Here is a nice analogy of the concept I have in mind. The funnel represents matter and the water represents space.

https://www.reddit.com/r/NatureIsFuckingLit/comments/dtaj2o/tiny_whirlpool_sucks_down_some_leaves/

Just like a flame consumes oxygen to burn and a light bulb needs electricty to glow so too does matter need to consume spacetime to exist and experience time.


***Force vs Gravity***

Velocity causes and object to move through space over time. Velocity is affected by force shown by f = ma.

But what we see as gravity has nothing to do with force or velocity. 

Two objects appear to attract one another and appear to move closer to one another via a force however that is just an illusion.

So how could the objects getting closer?

Neither of the objects are experience force and thus they have no velocity and are thus stationary.

How can they be both stationary and yet getting closer at the same time? Its a paradox

The answer is that the space itself between the objects is vanishing. 

Its getting sucked up by the two bodies themselves and thus they get closer together.


**Why is the expansion of the universe accelerating and not deccelerating like you would expect?**

Now based on this understanding there's a problem. 

If space is a finite substance that we are consuming simply by existing that means that it must be  diminishing.

So not only is space spreading out over time as the universe expands but the space left inside it is reducing. 

That means that time would have to start slowing as it starts running out.

However to an observer time has to be experienced at a constant rate therefore it would have to appear as if events on the edge of the universe were starting to speeding up.

Thankfully this is exactly what we've observed! 

The universe however isn't actually expanding faster, our space is getting thinner. We are slowing down and eventually our metaphorical river will dry up completely.

**The Cost of Change**

What is change in the physical sense of the word?

At a fundamental level change means movement over time.

But nothing comes for free in the universe, if you want to get something you have to give something, that is its fundamental nature.

So what does change cost?

Lets say for example you want to cross to the other side of the room. 

You can't just teleport there.

It would cost a couple of seconds of your time and a few meters of space.

Therefore the cost of change is literally spacetime.

Imagine standing outside in the rain with a bucket. 

You were allowed to move around but any movement drained your bucket so if your bucket was empty you would have to stand still and wait to fill up again.

Now what would happen if you were dropped out of a very high plane and started falling as fast as the rain? 

None of the rain would be able to land in your bucket and you would not be able to move.

This is what causes relativity. 



r is the radius of the planet
d is the distance away from the planet

spacetime radius = sqrt(r^2 + d^2)






